import java.util.ArrayList;

public class ApplicationMain {
	public static void main(String arg[]) {
	Product product=new Product();
	product.productName="pen";
	product.price=(double)5;
	product.quantity=60;
	product.category.name="Stationary";
	product.variation.variationList=new ArrayList<String>();
	product.variation.variationList.add("Red Color");
	product.printProductDetails();
	}
}