public class Product {
	public String productName;
	public Double price;
	public Integer quantity;
	public Category category=new Category();
	public Variation variation=new Variation();
	public void printProductDetails() {
		System.out.println("Product [productName=" + productName + ",price=" + price + "," + " quantity=" + quantity + "]; Category[" + category +"]; Variation[" + variation + "];");
	}

}